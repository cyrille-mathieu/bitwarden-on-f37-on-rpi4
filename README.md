# Installing Fedora Server 37/38 headless on Raspberry Pi 4B

**Update 2023/11/30:** This project is no longer actively maintained. There's a new repo for a Fedora Server 39 headless install on Raspberry Pi 4B [here](https://gitlab.com/cyrille-mathieu/headless-fedora-server-39-with-rpi-4).

**Note:** *This project was originally created after the release of Fedora 37 Server and has been reviewed for Fedora 38 Server as of 2023/06/24.*

There are very few, if any, changes between the two Fedora versions. Notable differences will have an extra comment. Filenames and version numbers (37/38) have mostly been left as they were. If you are working with Fedora 38 Server, just replace the version number accordingly. I have tested this full setup again with Fedora 38 Server.

## Table of Contents

- [1.0 Introduction](#introduction)
    - [1.1 Prerequisites](#prerequisites-for-this-project)
- [2.0 Preparing Fedora 37 Server for headless install](#20-preparing-fedora-37-server-for-headless-install)
    - [2.1 Acquiring the image](#21-acquiring-the-image)
    - [2.2 Mounting the image](#22-mounting-the-image)
    - [2.3 Modifying the image](#23-modifying-the-image)
    - [2.4 Cleaning up `chroot`](#24-cleaning-up-chroot)
    - [2.5 Installing the image](#25-installing-the-image)
    - [2.6 Testing it out in a VM](#26-testing-it-out-in-a-vm)

## 1.0 Introduction

This repository serves as a write-up to an idea I had: setting up a Bitwarden self-hosted password manager. More specifically, I wanted to work out this project on a Raspberry Pi 4B I had lying around and I wanted to use Fedora Linux.

While this might seem relatively straightforward, I had one extra constraint: I don't have a micro HDMI to HDMI connector for my RPI. The simple solution to this would be to acquire the appropriate cable, but that is not nearly as fun (and frustrating) as doing a headless install. So this write-up will include a headless installation of Fedora 37 Server on the RPI 4B.

I wanted to perform a headless AND wireless installation (over Wi-Fi) of Fedora 37 Server on the RPI. I didn't get this far and, as it turns out, a headless installation was already difficult enough for me at this stage. Feel free to add/fork this repository with instructions on how to get the wireless connectivity working on a headless install.

The write-up will be split into multiple parts, part one being the preparation of the OS. This section will be updated once I have finished the other parts of this project.

**Note:** This write-up is heavily based on [Roberto Meléndez](https://medium.com/geekculture/how-to-install-fedora-on-a-headless-raspberry-pi-62adfb7efc5)' Medium article about setting up a headless RPI with Fedora 34 Server. When following Roberto's article, I encountered some differences and problems with setting up Fedora 37 Server for a headless install. I will try to point these out and address these in this write-up. Roberto's article does contain a section at the end about getting Wi-Fi to work and the bug he refers to there, [has been resolved](https://bugzilla.redhat.com/show_bug.cgi?id=1756488).

### 1.1 Prerequisites used in this project:

- Raspberry Pi 4B (4GB)
- 32GB SDXC card
- Wired internet connectivity for the RPI
- Fedora 37 Server aarch64 raw image
- As a host system: a working installation of Fedora 36 Workstation or greater with KVM & QEMU

## 2.0 Preparing Fedora 37 Server for headless install

The high-level overview of the steps in this chapter are as follows:
1) Acquire image
2) Decompress image
3) Mount the image
4) Modify image
5) Compress image
6) Write image to disk

### 2.1 Acquiring the image

The RPI 4B has a [BCM2711](https://www.raspberrypi.com/documentation/computers/processors.html#bcm2711) SoC, the CPU is an [ARM Cortex-A72](https://en.wikipedia.org/wiki/ARM_Cortex-A72) which implements the [ARMv8](https://en.wikipedia.org/wiki/ARM_architecture_family#Armv8) core and uses ARM's AArch64 architecture. This means we need to download the Fedora 37/38 Server raw image for aarch64. Download it with your favourite CLI tool from the the following location:

For Fedora 37 Server:

> `wget https://download.fedoraproject.org/pub/fedora/linux/releases/37/Server/aarch64/images/Fedora-Server-37-1.7.aarch64.raw.xz`

For Fedora 38 Server:

> `wget https://download.fedoraproject.org/pub/fedora/linux/releases/38/Server/aarch64/images/Fedora-Server-38-1.6.aarch64.raw.xz`

**Note:** Please keep in mind that the location and image may have changed from the time this was written. If so, have a look for yourself [here](https://fedoraproject.org/server/) where you can download the OS image.

Before we can work with the image, we have to decompress it like so:

> `xz -d ./Fedora-Server-37-1.7.aarch64.raw.xz`

Roberto's article goes a bit deeper into what the resulting `.raw` file is made up of, feel free to investigate for yourself with:

> `fdisk -l ./Fedora-Server-37-1.7.aarch64.raw`

### 2.2 Mounting the image

Before we can boot Fedora 37 Server in a headless environment, we need to modify it. More specifically, we need to modify the partition with id `8e` from our raw image. However, before we can modify it, we need to mount the image locally.

We cannot mount an LVM partition directly, but `kpartx` helps us out here in creating a device mapping from the partition table in the `.raw` image:

> `sudo kpartx -av ./Fedora-Server-37-1.7.aarch64.raw`

Have a look with:

> `ls -l /dev/mapper/`

Normally you should see 3 loop devices and a Fedora root partition.

> **Note**: Roberto's article shows `fedora_fedora-root` as the root partition name, in Fedora 37 Server this is `fedora-root` under `/dev/mapper/`.

Create a directory and mount the Fedora root partition:

> `sudo mkdir /mnt/fedora` <br>
> `sudo mount /dev/mapper/fedora-root /mnt/fedora`

> **Note**: Roberto's article has a typo I presume in the `mount` command above. Make sure to use the correct partition name for your case.

To check that the Fedora root partition was mounted correctly, use `lsblk` and have a look at the mountpoint. If it is not mounted in the correct location, unmount it with:

> `sudo umount /mnt/fedora`

And mount it correctly to your location of choice.

In my case the terminal output looked like this:

```text
NAME                                          MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
loop0                                           7:0    0     7G  0 loop  
├─loop0p1                                     253:1    0   600M  0 part  
├─loop0p2                                     253:2    0     1G  0 part  
└─loop0p3                                     253:3    0   5.4G  0 part  
  └─fedora-root                               253:4    0   5.4G  0 lvm   /mnt/fedora
```

### 2.3 Modifying the image

We will edit the image in a `chroot` environment, before we can do so make sure that the `qemu-user-static` package is installed.

On RHEL distros, you can install it with:

> `sudo dnf install qemu-user-static`

Make sure that the `systemd-binfmt` service is running. If not, start it:

> `sudo systemctl enable --now systemd-binfmt`

Chroot to the mounted Fedora root partition and open a Bash shell in it:

> `sudo chroot /mnt/fedora /bin/bash`

Verify the environment's processor type is `aarch64`:

> `uname -p`

**Note:** Using the Fedora 38 image, this didn't work for me, instead, I used the `-m` option.

We will create a user with root permissions, this will also be the user we will use to manage the system later on. I went with the very creative username `pi`, feel free to choose the username you desire.

> `groupadd <username>` <br>
> `useradd -g <username> -G wheel -m -u 1000 <username>`

#### 2.3.1 Setting the user password

Roberto's article doesn't set a password for the freshly created user and adds an SSH key to the image and logs in that way when the system will boot for the first time. While this should work, it didn't for me. Some readers commented on his article that they encountered the same problem and were left with a system they could not log in to. 

Set a password for the newly created user:

`passwd <username>`

> **Note:** If the `passwd` command does not return a permission error, skip ahead to the next chapter [2.3.2](#232-setting-ssh-keys). Else, follow the following bit.

Executing the `passwd` command above presented me with the following errors:

> `qemu-aarch64-static: qemu_mprotect__osdep: mprotect failed: Permission denied` <br>
> `qemu-aarch64-static: mprotect of jit buffer: Permission denied`

I tested this on Fedora 36, 37 and 38 Workstation and I got the same errors. If you're running a RHEL-like OS as a host, I found that temporarily setting SELinux to `Permissive` mode allowed me to set the user's password. Check that SELinux is in `Enforcing` mode with `getenforce`. If it is, execute the following on your host (exit the `chroot` environment first):

> `sudo setenforce 0`

Check again with `getenforce` to make sure your change was applied correctly.

Then, `chroot` like before and set the newly created user's password as **root**. If you try to `su - <user>` and then `passwd` in the `chroot` environment you will probably get:

> `passwd: Authentication token manipulation error`

**IMPORTANT:** For the sake of security, exit the `chroot` environment now, enable SELinux again with:

> `sudo setenforce 1`

Check that SELinux is `Enforcing` again and `chroot` back like before.

#### 2.3.2 Setting SSH keys

SSH keys are are useful and secure tool for logging in to (remote) machines, the following command creates the `.ssh` directory and `authorized_keys` file and sets the correct permissions for both:

> `mkdir /home/<username>/.ssh && chmod 700 $_ && touch $_/authorized_keys && chmod 600 $_`

Set the correct ownership:

> `chown -R <username>:<username> /home/<username>/.ssh/`

Add your **public** (`.pub`) SSH key to the `/home/<username>/.ssh/authorized_keys` file. To do this, open another terminal and `cp` your public SSH key from your host to the `chroot` environment.

#### 2.3.3 Unlinking the initial setup services

Upon the first boot of the RPi, some `systemd` targets will look for specific services. Disable these:

> `unlink /etc/systemd/system/multi-user.target.wants/initial-setup.service` <br>
> `unlink /etc/systemd/system/graphical.target.wants/initial-setup.service`

### 2.4 Cleaning up `chroot`

Exit the `chroot` environment and unmount the Fedora root partition:

> `sudo umount /mnt/fedora && sudo rmdir $_`

Just like we mapped the `.raw` disk image's partitions with `kpartx`, we need to unmap these. If we tried the following:

> `sudo kpartx -d ./Fedora-Server-37-1.7.aarch64.raw`

and checked with `lsblk`, we would see that the Fedora root partition is not unmapped. Since this is a LVM volume, we need to deactivate it first. Scan the physical volumes first:

> `sudo pvscan`

Following that, deactivate the volume group:

> `sudo vgchange -a n fedora`

And finally, unmap the disk image:

> `sudo kpartx -d ./Fedora-Server-37-1.7.aarch64.raw`

Check with `lsblk` to make sure all partitions were removed.

### 2.5 Installing the image

Compress the disk image, read the `xz` man pages if you need more information for the parameters:

> `xz -zk -T0 ./Fedora-Server-37-1.7.aarch64.raw`

> **Note:** I recommend including the `-k` parameter in the command above, this *keeps* the original `.raw` disk image. This can prove to be useful if you need to make some (quick) changes to the image. Alternatively, store a copy of the compressed image for your own, later reference.

At this point I didn't know if the prepared image would work on the RPI (remember, I didn't have video output to troubleshoot the boot process). Thus, I came up with the idea of testing it in a VM first.

If you're reading this write-up with the sole intent of obtaining a headless Fedora 37 Server installation ready for your Raspberry Pi 4B, follow chapter [2.5.1](#251-writing-the-image) and 2.5.2. Otherwise, follow chapter [2.6](#26-testing-it-out).

#### 2.5.1 Writing the image

Connect your SD(XC) card to your computer and find out where it is mounted with `lsblk`.

Make sure the `arm-image-installer` package is installed on your system for the next step. The following command writes the prepared image to your SD card:

> `sudo arm-image-installer --image=./Fedora-Server-37-1.7.aarch64.raw.xz --media=/dev/sdX --target=rpi4 --norootpass --resizefs -y`

Once this is done (it can take a couple minutes), safely disconnect the SD card from your machine, insert the micro SD card into your RPI and make sure your RPI has a wired internet connection and can receive an IP address via DHCP.

Lastly, power on your RPI.

#### 2.5.2 Connecting to your headless Raspberry Pi

If all goes well, your RPI should now be booting Fedora 37 Server. The `sshd.service` and `cockpit.service` should both come online automagically and you have two ways of connecting to your RPI: via an SSH client and/or via HTTPS in a web browser.

Obviously, you first need to know the IP address of your RPI, which you can obtain by:

1) Checking the connected devices via your router
2) Viewing the active leases if you have configured a separate DHCP server on your network
3) Scanning your ***local*** network with a tool like `nmap`

Once you have the IP of your RPI, connect:

1) via CLI: `ssh <username>@<RPI-IP-address>`
2) via GUI: `https://<RPI-IP-address>:9090/`

Just like its RHEL counterparts, Fedora Server editions come with a built-in Cockpit to manage your server via a web interface if you prefer/need a GUI.

### 2.6 Testing it out in a VM

//